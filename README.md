Tor config and init script for Turris Omnia


**cpfiles.sh** - is used for sync tor-conf with your router

Commands:
* copy - Copy files to router
* get -  Copy files from router

**src/tor.conf** - uci tor configuration

**src/tor.sh** - new init.d script for tor

**src/utils.sh** - helper functions for init script
